/* Copyright (C) 2019 Casper Meijn <casper@meijn.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12

ApplicationWindow {
    property url source: "https://www.sample-videos.com/video123/mp4/720/big_buck_bunny_720p_5mb.mp4"
//    property url source: "file:///sdcard/SampleVideo_1280x720_5mb.mp4"
    property bool qtMultimediaAvailable: true
    property bool qtAVAvailable: true

    id: window
    visible: true
    width: 640
    height: 480
    title: qsTr("qt-android-video-example")

    Component.onCompleted: {
        console.log("qt-android-video-example started");
    }

    ColumnLayout{
        anchors.fill: parent

        TabBar {
            id: bar
            Layout.fillWidth: true
            TabButton {
                text: qsTr("Home")
            }
            TabButton {
                text: qsTr("QtMultimedia")
                enabled: qtMultimediaAvailable
            }
            TabButton {
                text: qsTr("QtAV")
                enabled: qtAVAvailable
            }
        }

        StackLayout {
            Layout.fillHeight: true
            Layout.fillWidth: true
            currentIndex: bar.currentIndex

            ColumnLayout{
                Text {
                    Layout.fillWidth: true
                    text: qsTr("This is a example application for showing of different Qt5 video components. Select one of the other tabs to start a video.")
                    wrapMode: Text.WordWrap
                    padding: 10
                }
                Text {
                    Layout.fillWidth: true
                    text: qsTr("Selected source: ") + source
                    wrapMode: Text.WordWrap
                    padding: 10
                }
            }

            Loader {
                active: qtMultimediaAvailable
                source: "qtMultimediaPage.qml"
            }

            Loader {
                active: qtAVAvailable
                source: "qtAVPage.qml"
            }
        }
    }
}
