# qt-android-video-example

This is an example application which shows the Video component from different multimedia frameworks. I needed this after discovering that the standard QtMultimedia doesn't support RTSP on Android. The default Android video component mainly supports local media. This example application has a few tabs, where each tab shows a simple Video component.

## Android
You can build this application using the `build-android.sh` script. This will create a docker container and build the application in it. The APK is then available in `build-android` directory.

The video source is defined in `main.qml`. You can change this to the sd-card and give the application permissions to access it. I had to restart the application before the permissions were fully applied.
