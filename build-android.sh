#/bin/bash
set -eu

mkdir -p build-android
podman build -t docker-android-build docker-android-build
podman run -it --volume $PWD:/home/user/src/qt-android-video-example:ro,z --volume $PWD/build-android:/output:z docker-android-build /bin/bash -c '
/opt/helpers/build-cmake qt-android-video-example invalid-git-repo -DANDROID_APK_DIR=/home/user/src/qt-android-video-example/android -DQTANDROID_EXPORTED_TARGET=qt-android-video-example -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON -DCMAKE_MODULE_PATH=/opt/kdeandroid-deps/lib/cmake/ &&
cd build/qt-android-video-example && 
make create-apk && 
cp qt-android-video-example_build_apk/build/outputs/apk/debug/qt-android-video-example_build_apk-debug.apk /output'
